import 'dart:async';

import 'package:flappybird/barriers.dart';
import 'package:flappybird/bird.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double time = 0;
  double initialHeight = 0;
  double height = 0;
  double birdYaxis = 0;
  bool gameHasStarted = false;
  static double barrierXone = 1;
  double barrierXtwo = barrierXone + 2;
  void jump() {
    setState(() {
      time = 0;
      initialHeight = birdYaxis;
    });
  }

  void startGame() {
    gameHasStarted = true;
    Timer.periodic(const Duration(milliseconds: 60), (Timer) {
      time += 0.05;
      height = -4.9 * time * time + 2.8 * time;
      setState(() {
        birdYaxis = initialHeight - height;
      });

      setState(() {
        if (barrierXone < -2) {
          barrierXone += 4;
        } else {
          barrierXone -= 0.05;
        }
      });
      setState(() {
        if (barrierXtwo < -2) {
          barrierXtwo += 4;
        } else {
          barrierXtwo -= 0.05;
        }
      });
      if (birdYaxis >= 1) {
        Timer.cancel();
        gameHasStarted = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (gameHasStarted) {
          jump();
        } else {
          startGame();
        }
      },
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              flex: 3,
              child: Stack(
                children: [
                  AnimatedContainer(
                    color: Colors.blue,
                    duration: const Duration(milliseconds: 0),
                    alignment: Alignment(0, birdYaxis),
                    child: const MyBird(),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 0),
                    child: const Barriers(size: 150),
                    alignment: Alignment(barrierXone, 1.1),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 0),
                    child: const Barriers(size: 150),
                    alignment: Alignment(barrierXone, -1.1),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 0),
                    child: const Barriers(size: 200),
                    alignment: Alignment(barrierXtwo, 1.1),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 0),
                    child: const Barriers(size: 100),
                    alignment: Alignment(barrierXtwo, -1.1),
                  ),
                  gameHasStarted
                      ? const Text("")
                      : Container(
                          alignment: const Alignment(0, -0.3),
                          child: const Text(
                            "T A P   T O   P L A Y",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ),
                ],
              ),
            ),
            Container(
              color: Colors.green,
              height: 15,
            ),
            Expanded(
              child: Container(
                  color: Colors.brown,
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "Score",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                          Text(
                            "0",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "Best",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                          Text(
                            "10",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          )
                        ],
                      )
                    ],
                  )),
              flex: 1,
            ),
          ],
        ),
      ),
    );
  }
}
